---
layout: post
title:  "My first post"
date:   2018-01-15 15:33:26 +0200
categories: jekyll test
published: true
---

Das hier ist mein erster Beitrag. Da schreibe ich noch etwas dazu.
Dann schreibe hier auch noch kurz was dazu. Test[^1].

Und hier möchte ich ein bisschen Mermaid[^2] testen:

https://twitter.com/DASKAjA/status/349605640290844672

# Stable Diagram Types

## Sequence Diagram

{% stylesheet mermaid %}

{% mermaid %}
sequenceDiagram
    participant Alice
    participant Bob
    Alice->>John: Hello John, how are you?
    loop HealthCheck
        John->>John: Fight against hypochondria
    end
    Note right of John: Rational thoughts <br/>prevail...
    John-->>Alice: Great!
    John->>Bob: How about you?
    Bob-->>John: Jolly good!
{% endmermaid %}

## Graph Diagram

{% mermaid %}
graph TD
A[Christmas] -->|Get money| B(Go shopping)
B --> C{Let me think}
C -->|One| D[Laptop]
C -->|Two| E[iPhone]
C -->|Three| F[Car]
{% endmermaid %}

{% mermaid %}
graph TD;
    A-->B;
    A-->C;
    B-->D;
    C-->D;
{% endmermaid %}

## Gant Chart

{% mermaid %}
gantt
        dateFormat  YYYY-MM-DD
        title Adding GANTT diagram functionality to mermaid
        section A section
        Completed task            :done,    des1, 2014-01-06,2014-01-08
        Active task               :active,  des2, 2014-01-09, 3d
        Future task               :         des3, after des2, 5d
        Future task2               :         des4, after des3, 5d
        section Critical tasks
        Completed task in the critical line :crit, done, 2014-01-06,24h
        Implement parser and jison          :crit, done, after des1, 2d
        Create tests for parser             :crit, active, 3d
        Future task in critical line        :crit, 5d
        Create tests for renderer           :2d
        Add to mermaid                      :1d
{% endmermaid %}

# Experimental Diagram Types

## Class Diagram

{% mermaid %}
classDiagram
Class01 <|-- AveryLongClass : Cool
Class03 *-- Class04
Class05 o-- Class06
Class07 .. Class08
Class09 --> C2 : Where am i?
Class09 --* C3
Class09 --|> Class07
Class07 : equals()
Class07 : Object[] elementData
Class01 : size()
Class01 : int chimp
Class01 : int gorilla
Class08 <--> C2: Cool label
{% endmermaid %}

## git Diagram

{% mermaid %}
gitGraph:
options
{
    "nodeSpacing": 150,
    "nodeRadius": 10
}
end
commit
branch newbranch
checkout newbranch
commit
commit
checkout master
commit
commit
merge newbranch
{% endmermaid %}

und etwas PHP:

```php
<?php
phpinfo();
```

[^1]: Dies ist eine Fussnote. Ein Eszet konnte ich mit VSCode aus irgendeinem Grund nicht schrieben.
[^2]: Mermaid ist eine DSL zum erstellen von Diagrammen.