---
title: Run various PHP versions side-by-side via Docker using CodeRunner on macOS 
published: true
description: I struggle to use various PHP versions in parallel on my Mac. So I investigated how I can run them via CodeRunner and Docker. Turns out it's not that hard to get it working.
tags: php, docker, macos, workflow
layout: post
date:   2017-10-26 15:33:26 +0200
lang: en_US
author: Jens Kohl
---

I'm doing a lot of small things in PHP these days, non of them is actual web development. The environments in which these scripts are meant to run have various PHP versions installed (I'm looking at you, CentOS LTS). Since most of my tasks involve short PHP scripts it isn't worth it to fire up [phpStorm](https://www.jetbrains.com/phpstorm/) and setup a whole project.

So I went to my go-to tool for small scripts: [CodeRunner](https://coderunnerapp.com), but right out of the box it only supports one version at a time for PHP, since it uses the pre-installed PHP which comes with macOS High Sierra fairly outdated. You can install other PHP versions via [Homebrew (package manager)](https://brew.sh/) but using them in parallel isn't really a pleasure. I prefer using [Docker](https://www.docker.com/) for that. And that's what we gonna do, so let's start configuring CodeRunner:

# Setting up CodeRunner

* First go to the settings (`⌘;`) → Languages and search for `PHP` in the list on the left.
* Duplicate `PHP` with the »Settings«-Button below that list ![duplicate the PHP language](https://thepracticaldev.s3.amazonaws.com/i/0luu4z7txre5jgk2ms01.png)
* Rename it to something meaningful, like `PHP 7.1 via Docker`
* For the **Run Command** enter

```bash
docker run --rm -v $(pwd):/opt/project php:7.1 -f /opt/project/$filename
```

What this does is running a new container from the image named `php:7.1`. `-v $(pwd):/opt/project` mounts the current directory in which CodeRunner will run your code into `/opt/project` inside of the container.
`-f /opt/project/$filename` tells the php binary living inside of the container to run the script named `/opt/project/$filename`.
`$filename` is actually a placeholder for the filename CodeRunner will (temporarily) save your code to.
And last but not least, `--rm` will ensure, that after CodeRunner executed your code, it will remove the just created container from your system again.
That has also the benefit, that every time you run your code within CodeRunner it gets executed in a fresh environment (everything you save from your code above the `/opt/project` directory gets wiped)

* Close the settings window and bring the CodeRunner window to the front
* In the toolbar, there is a dropdown subtitled »Language«. Choose your newly created PHP environment labeled as `PHP 7.1 via Docker`. ![Choose PHP 7.1 via Docker from Languages dropdown](https://thepracticaldev.s3.amazonaws.com/i/gzc2ajr46w24b5gbk1ij.png)

# Setting up Docker for Mac

Before we can start, we have to give **Docker for Mac** permission to mount (the `-v` flag) the directory where CodeRunner places its temporary files. To do that

* Open Docker for Mac »Preferences…« via its menu item ![Open Docker for Mac Preferences via its menu item](https://thepracticaldev.s3.amazonaws.com/i/wip7q9v104oxrkzbsyuu.png)
* Go to the `File Sharing` tab and click the `+` button
* This will open the system open dialog. It doesn't really matter which directory you open, so you just click the »Open« button in the bottom right
* The newly create list item you have to double click. Now you can modify it
* Enter `/var/folders/pc` and press return (↩) ![Docker for Mac after adding /var/folders/pc to the file sharing tab](https://thepracticaldev.s3.amazonaws.com/i/g4k5t355p6l2e08t693g.png)

# Bringing it all together

You're no good to go, enter something like the script below to test your newly created CodeRunner docker environment:

```php
<?php
echo 'PHP ' . phpversion() . PHP_EOL;
```

When you're using the `php:7.1` Docker image for the first time it needs to download it from the Docker Hub first, this can take some seconds to minutes, depending on your internet connection. It will look like the following:

```
Unable to find image 'php:7.1' locally
7.1: Pulling from library/php

85b1f47fba49: Already exists
66e22dddbf92: Already exists
bf0df491fd2e: Already exists
0cbe7899c5b5: Already exists
515aeb1bd86c: Already exists
842bd485599e: Already exists
Digest: sha256:9d847a120385a1181ffa8ba4d17f28968fb2285923a0ca690b169ee512c55cb1
Status: Downloaded newer image for php:7.1
```

And after that you'll get the output of your PHP script: ![Running PHP 7.1 with CodeRunner via Docker](https://thepracticaldev.s3.amazonaws.com/i/jchf51jyp4066ljqlzij.png)

# What to do next?

Congratulations, you're done and can run your short PHP scripts via Docker in CodeRunner. You can clone the `PHP 7.1 via Docker` environment for other PHP versions. Right now there are several versions in the official PHP docker image, even the latest pre-release versions of PHP 7.2.

But this approach is not limited to PHP. I guess you can setup something similar for your python, node.js and ruby environments.

But there's one caveat: You have to read the corresponding `Dockerfile` to find out where to set the mount point (it's probably not `/opt/project`) in the image, so that when running the docker container you find your files you want to execute.
