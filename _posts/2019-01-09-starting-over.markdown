---
layout: post
title:  "Starting over"
date:   2019-01-09 13:07:14 +0100
categories: blog
---

It’s been a while, since I tried to build a blog with static site generator. But it's the beginning of a new year and I want to try it again. So no with some more docker images, and deployment providers I will start a new attempt to get back into blogging, after my last blog closed over a decade ago. There will be tech-related blog post, possible cross posted to [dev.to](https://dev.to) and it will be in english.