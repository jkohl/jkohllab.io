#!/bin/bash
set -x -e
docker run --rm -it --volume="$PWD:/srv/jekyll" --volume="$PWD/vendor/bundle:/usr/local/bundle" -p 4000:4000 -p 35729:35729 -p 3000:3000 -e JEKYLL_ENV=development jekyll/jekyll:latest bundle exec jekyll build